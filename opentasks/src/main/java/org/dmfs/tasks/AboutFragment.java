package org.dmfs.tasks;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceFragment;

import androidx.annotation.Nullable;


public class AboutFragment extends PreferenceFragment {

    public static final String BUILD_VERSION = "build_version";
    public static final String APP_INFO = "tasks_info";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.about_preferences);
        findPreference(BUILD_VERSION).setSummary(BuildConfig.VERSION_NAME);
        findPreference(APP_INFO).setSummary(getString(R.string.preferences_app_info));


    }
}
